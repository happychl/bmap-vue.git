export default function () {
  return class ResetControl extends BMapGL.Control {
    constructor(options) {
      super();

      // eslint-disable-next-line
      this.defaultAnchor = options?.anchor ?? BMAP_ANCHOR_BOTTOM_RIGHT;
      this.defaultOffset = options?.offset ?? new BMapGL.Size(15, 80);
    }

    initialize(map) {
      let div = document.createElement('div');
      div.className = 'BMap_stdMpReset BMap_noprint anchorBR';

      let btn = document.createElement('div');
      btn.className = 'BMap_button_new BMap_stdMpResetBtn';

      let icon = document.createElement('img');
      icon.src = require('./resize.svg');

      btn.appendChild(icon);
      div.appendChild(btn);

      div.onclick = function (e) {
        map.viewportReset();
      };

      map.getContainer().appendChild(div);

      return div;
    }
  };
}
