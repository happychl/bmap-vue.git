// 观察者模式
export default class Observer {
    on(type, fn) {
        if (!this.observer) {
            this.observer = {};
        }
        if (!this.observer[type]) {
            this.observer[type] = [];
        }
        this.observer[type].push(fn);
    }

    emit(type, ...args) {
        if (!this.observer || !this.observer[type]) return;
        this.observer[type].forEach(cb => {
            cb.apply(this, args);
        });
    }
}
