# `bmap-vue`

> 封装的百度地图`vue`组件

## 使用说明

### 安装

```bash
npm install bmap-vue
# OR
yarn add bmap-vue
```

### 使用

```js
// 导入组件
import BdMap from 'bmap-vue';

// 模板中使用
<bd-map :options="options" @loaded="mapLoaded" />;

// 全局挂载（按需添加）
Vue.use(BdMap);
```

### 参数

#### `options`

| 属性           | 类型           | 默认值    | 描述                                                         |
| -------------- | -------------- | --------- | ------------------------------------------------------------ |
| ak             | String         |           | 百度地图`ak`                                                 |
| lng            | String\|Number | 116.39167 | 中心点经度                                                   |
| lat            | String\|Number | 39.90333  | 中心点维度                                                   |
| zoom           | Number         | 10        | 初始缩放级别                                                 |
| minZoom        | Number         | 5         | 最小缩放级别                                                 |
| maxZoom        | Number         | 18        | 最大缩放级别                                                 |
| style          | String\|Object |           | 地图样式，支持`styleId`字符串或`styleJson`对象               |
| disableControl | Boolean        | false     | 是否禁用地图控件                                             |
| useTilesLoaded | Boolean        | false     | 使用瓦片加载完成事件（瓦片加载完再显示地图，可避免白屏闪烁） |
| enableVgl      | Boolean        | false     | 是否开启`mapvgl`功能                                         |
| enableCluster  | Boolean        | false     | 是否开启地图聚合功能                                         |
| extra          | Object         |           | 额外的初始化配置                                             |

### 事件

| 名称   | 参数 | 备注                             |
| ------ | ---- | -------------------------------- |
| loaded | map  | 地图加载完成回调，参数为地图实例 |

### 实例方法

| 名称                                                                                | 返回值 | 备注                                                                    |
| ----------------------------------------------------------------------------------- | ------ | ----------------------------------------------------------------------- |
| parsePoint(\<Array\|Point>point)                                                    | Point  | 将经纬度数组`[lng, lat]`转换为百度`Point`点位                           |
| setReset(\<Array\|Point>center, \<Number>zoom)                                      |        | 设置重置点（地图`reset`控件重置的点位）                                 |
| viewportReset()                                                                     |        | 视窗重置（比自带的`reset`方法更精准）                                   |
| setCenterAndZoom(\<Array\|Point>center, \<Number>zoom, \<Boolean>animation = false) |        | 设置地图中心点和缩放，`animation`为是否启用动效，默认不启用             |
| setMapStyle(\<String\|Object>style)                                                 |        | 设置地图样式，支持`styleId`字符串或`styleJson`对象                      |
| openSatelliteLayer(\<Number>type)                                                   |        | 开启卫星图层，1：自定义卫星图层，2：百度地球，3：百度卫星图，默认值为 1 |
| closeSatelliteLayer()                                                               |        | 关闭卫星图层                                                            |
| removeOverlays(\<Overlay>overlay,[...])                                             |        | 批量移除地图覆盖物                                                      |

### 其他

1. 地图实例支持所有百度地图原生`api`；
2. 已启用并自动挂载[`bmap-libs`](https://www.npmjs.com/package/bmap-libs)插件；
