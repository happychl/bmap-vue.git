import BdMap from './bdMap/index.vue';

BdMap.install = function (Vue) {
  Vue.component(BdMap.name, BdMap);
};

export default BdMap;
